# My Minicommerce.

El propósito de este proyecto es crear una app
que permita vender un producto o servicio tantas
veces sea necesario, que se pueda pagar mediante
una billetera virtual y que un administrador de la
aplicación pueda gestionar estas transacciones.

El desarrollo de aplicación tendrá en detalle
las siguientes tareas:

- [x] Definición de la arquitectura y búsqueda 
de los patrones de diseño para la solución. (3 h).
  - Se encontro una aplicación de un carrito de
    compras en Python [en github](https://github.com/muvatech/Shopping-Cart-Using-Django-2.0-and-Python-3.6), se puede usar una
    arquitectura MVC y se puede solucionar el 
    problema de la venta del producto con [un decorador](https://es.wikipedia.org/wiki/Decorator_(patr%C3%B3n_de_dise%C3%B1o)#Ejemplo_Python) de
    esta manera la solución seria simple sin perder
    elegancia. 

    Estimado | Real/Consumido hasta el momento.
    -------- | ---------
    3h | 30 min


- [x] Búsqueda de recursos en la web; código para reusar, plataformas donde desplegar, etc. (3 h).
  - Como se usará Python, podemos usar [pythonanywhere](https://www.pythonanywhere.com/) para desplegar nuestra aplicación.  

  Estimado | Real/Consumido hasta el momento
    -------- | ---------
    3h | 5 min

    - [ ] Crear la cuenta en el portal.

- [ ] Lectura y pruebas de la documentación de la billetera (2 h).
- [ ] Creación de la prueba de concepto para la
aplicación de minicomercio y el manejo de usuarios; descargar las plantillas, crear el projecto web, establecer 
los scripts de pruebas, crear la base de datos,
la capa del modelo, el acceso a datos, unir las
vistas con las plantillas y desplegar (8 h).
- [ ] Creación de la prueba de concepto para el
uso de los _Mobile_ _deep_ _linking_ y el 
_widget_ para el consumo de la API (3 h).
- [ ] Listado de operaciones y solicitudes de 
pago para el posible retorno del pago (2 h).
- [ ] Desarrollar y probar los componentes
opcionales y adicionales como manejo de lenguajes (2 h).

# Temas a mejorar o recomendaciones.
